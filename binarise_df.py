import pandas as pd
import numpy as np

# For working on an aggregated dataset

# d = pd.read_csv('perception_cognition_agg.csv', sep = '\t')
d = pd.read_csv('clics2_agg_perception_cognition.csv', sep = '\t')
all_glosses = set()

for el in d['Concepticon_Gloss']:
    el = el.strip().split(',')
    for subel in el:
        all_glosses.add(subel)

# The order of insertion is maintained for iteration
# from Py 3.7 if we don't delete anything.

# Had to revert to Python 3.6 on Ubuntu

all_glosses_sorted = sorted(all_glosses)

glosses_indices = { val: idx for idx, val in enumerate(all_glosses_sorted) }
colnames = list(all_glosses_sorted)

bin_mat = np.zeros((d.shape[0], len(colnames)), int)

for i in range(d.shape[0]):
    r = d.iloc[i,:]
    glosses = r['Concepticon_Gloss'].strip().split(',')
    for g in glosses:
        idx = glosses_indices[g]
        bin_mat[i,idx] = 1
        
agg_plus_binary = np.hstack((d, bin_mat))
bin_agg = pd.DataFrame(agg_plus_binary)
bin_agg.columns = ['Name', 'Macroarea', 'Family', 'clics_form', 'glosses'] + colnames
bin_agg.to_csv('agg_bin_new.csv', sep = '\t', index = False)

# For working on a non-aggregated dataset

# import pandas as pd
# import numpy as np

# print("Loading data...")

# d = pd.read_csv('clics.csv')

# all_glosses = set()

# for el in d['Concepticon_Gloss']:
#     el = el.strip()
#     all_glosses.add(el)

# print("Creating indices...")
    
# glosses_indices = { val: idx for idx, val in enumerate(all_glosses) }
# colnames = list(glosses_indices.keys())

# form_gloss_dic = {}
# for r in d.iterrows():
#     r = r[1]
#     k = tuple(r[:4])
#     v = r['Concepticon_Gloss']
#     form_gloss_dic[k] = form_gloss_dic.get(k, []) + [v]

# # When you have enough memory
    
# # Trying to save memory as much as possible
# binary_mat = np.zeros((len(form_gloss_dic), len(colnames)),
#                       dtype = np.uint8)

# words = list(form_gloss_dic.keys())

# print("Filling binary matrix...")

# for i, w in enumerate(words):
#     for g in form_gloss_dic[w]:
#         j = glosses_indices[g]
#         binary_mat[i][j] = 1


# print("Saving to file...")
        
# np.savetxt('/Volumes/NO NAME/clics_binary.csv',
#         binary_mat,
#         delimiter = ',',
#         fmt = '%d')

# print("All done!")


# pd.DataFrame(words).to_csv('/Volumes/NO NAME/clics_meta.csv',
#                            index = False)

# np.savetxt('/Volumes/NO NAME/colnames.csv',
#         np.array(colnames),
#         delimiter = ',',
#         fmt = '%s')
