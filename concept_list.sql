SELECT LanguageTable.Name,
       LanguageTable.Macroarea,
       LanguageTable.Family,
       FormTable.clics_form,
       ParameterTable.Concepticon_Gloss
FROM FormTable
LEFT JOIN ParameterTable
ON
FormTable.Parameter_ID = ParameterTable.ID
AND
FormTable.dataset_ID = ParameterTable.dataset_ID
LEFT JOIN LanguageTable
ON
FormTable.Language_ID = LanguageTable.ID
AND
FormTable.dataset_ID = LanguageTable.dataset_ID
WHERE  LanguageTable.Name IS NOT NULL
       AND
       LanguageTable.Macroarea IS NOT NULL
       AND
       ParameterTable.Concepticon_Gloss IN (
                                           'HEAR',
                                           'FEEL',
                                           'KNOW OR BE ABLE',
                                           'KNOW (SOMEBODY)',
					                       'KNOW (SOMETHING)',
                                           'LISTEN',
                                           'LOOK FOR',
                                           'SMELL (PERCEIVE)',
                                           'TASTE (SOMETHING)',
                                           'UNDERSTAND',
                                           'GET',
                                           'MEET',
                                           'LOOK',
                                           'FIND',
                                           'READ',
                                           'SEE',
                                           'SEEM',
                                           'OBEY',
                                           'SENSE (PERCEIVE BY SENSES)',
                                           'BELIEVE',
                                           'THINK (BELIEVE)',
                                           'LEARN',
                                           'THINK (REFLECT)',
                                           'FIND OUT'
                                           )
