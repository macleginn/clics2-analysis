import pandas as pd
import networkx as nx
from itertools import combinations as combs

d = pd.read_csv('clics2_collex_new.csv', sep = '\t')

nodes = list(d)[5:]

# Global graph
G_global = nx.Graph()
d_bin = d.iloc[:, 5:]
for r in d_bin.iterrows():
    r = r[1]
    indices = []
    for idx, val in enumerate(r):
        if val:
            indices.append(idx)
    for c in combs(indices,2):
        if G_global.has_edge(nodes[c[0]], nodes[c[1]]):
            G_global[nodes[c[0]]][nodes[c[1]]]['weight'] += 1
        else:
            G_global.add_edge(nodes[c[0]], nodes[c[1]])
            G_global[nodes[c[0]]][nodes[c[1]]]['weight'] = 1
nx.write_gexf(G_global, 'sem_graph_global_new.gexf')
    
print(
    pd.DataFrame(sorted(nx.betweenness_centrality(G_global).items(),
    key = lambda x: x[1],
    reverse = True))
)

# Local graphs
for marea in ['Eurasia', 'Africa', 'Papunesia', 'South America']:
    d_marea = d.loc[d['Macroarea'] == marea]
    d_bin = d_marea.iloc[:,5:]
    G = nx.Graph()
    for r in d_bin.iterrows():
        r = r[1]
        indices = []
        for idx, val in enumerate(r):
            if val:
                indices.append(idx)
        for c in combs(indices,2):
            if G.has_edge(nodes[c[0]], nodes[c[1]]):
                G[nodes[c[0]]][nodes[c[1]]]['weight'] += 1
            else:
                G.add_edge(nodes[c[0]], nodes[c[1]])
                G[nodes[c[0]]][nodes[c[1]]]['weight'] = 1
    # print(marea)
    # print(
    #     pd.DataFrame(sorted(nx.betweenness_centrality(G).items(),
    #     key = lambda x: x[1],
    #     reverse = True))
    # )
    # print()
    nx.write_gexf(G, 'sem_graph_new_%s.gexf' % marea)
    # loc_comm = nx.communicability(G)
    # c_dict = {}
    # for c in combs(nodes,2):
    #     n1, n2 = c
    #     try:
    #         c_dict[(n1,n2)] = loc_comm[n1][n2]
    #     except:
    #         continue
    # print(
    #     '\n'.join(
    #         str(el) for el in sorted(c_dict.items(), key = lambda x: x[1], reverse=True)[:20]
    #     )
    # )


global_comm = nx.communicability(G_global)
gc_dict = {}
for c in combs(nodes,2):
    n1, n2 = c
    gc_dict[(n1,n2)] = global_comm[n1][n2]


print(
    '\n'.join(
        str(el) for el in sorted(gc_dict.items(), key = lambda x: x[1], reverse=True)[:20]
    )
)


for i in range(5):
    print(i)
    
